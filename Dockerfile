FROM python:3.10

ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY pyproject.toml* poetry.toml* poetry.lock* /code/

RUN pip install --upgrade pip
RUN pip install poetry
RUN poetry config virtualenvs.create false --local
RUN poetry install

RUN groupadd -g 1000 app
RUN useradd -u 1000 -ms /bin/bash -g app app

USER app
