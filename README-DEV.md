# README

- Build project

```bash
docker-compose build
```

- Build package

```bash
docker-compose run --rm app poetry build
```

- Publish package

```bash
docker-compose run --rm app poetry publish
```
